import os
import time
from load_files.models import *
from load_files.serializers import *
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from load_files.tasks import load_file_and_changing_status


class FileUploadAPIView(APIView):
    serializer_class = FileSerializer

    def post(self, request, *args, **kwargs):
        if 'file' not in request.FILES:
            return Response({'error': 'File not found'})
        else:
            uploaded_file = request.FILES.get('file')
            file_name = uploaded_file.name
            file_content = uploaded_file.read()
            load_file_and_changing_status.delay(file_content, file_name)
            return Response({'code': status.HTTP_201_CREATED,
                             'file': file_name})


class FileViewSet(ReadOnlyModelViewSet):
    queryset = File.objects.all()
    serializer_class = AllFileSerializer


class FileDeleteView(APIView):
    def post(self, request):
        try:
            record_id = request.data.get('id')
            record = File.objects.get(id=record_id)
            record.delete()
            return Response({'message': 'Record deleted successfully'}, status=status.HTTP_204_NO_CONTENT)

        except File.DoesNotExist:
            return Response({'error': 'Record not found'}, status=status.HTTP_404_NOT_FOUND)

        except Exception as e:
            return Response({'error': str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

